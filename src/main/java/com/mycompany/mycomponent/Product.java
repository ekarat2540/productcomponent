/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author ekara
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
            list.add(new Product(1, "espresso", 40, "1.jpg"));
        list.add(new Product(1, "espresso2", 40, "2.jpg"));

        list.add(new Product(1, "espresso3", 50, "3.jpg"));

        list.add(new Product(1, "espresso4", 10, "1.jpg"));

        list.add(new Product(1, "espresso5", 20, "3.jpg"));

        list.add(new Product(1, "espresso6", 60, "2.jpg"));

        list.add(new Product(1, "espresso7", 35, "2.jpg"));

        list.add(new Product(1, "espresso8", 45, "3.jpg"));

        list.add(new Product(1, "espresso9", 55, "1.jpg"));

        list.add(new Product(1, "espresso10", 50, "1.jpg"));

        return list;
     
    }
    
}
